- 👋 Hi, I’m Julia
- 👀 I’m interested in smart contracts and cryptography
- 🌱 I’m currently learning Python and how to stop breaking stuff
- 📫 How to reach me : contact@juliamarch.com

<!---
juliamrch/juliamrch is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
